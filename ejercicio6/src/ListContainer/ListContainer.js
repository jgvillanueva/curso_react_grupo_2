import React from 'react';
import './listContainer.scss';
import List from "../List/List";
import Detail from "../Detail/Detail";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from 'react-router-dom';

class ListContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <div className="listContainer">
                <h1>List container</h1>
                <Router>
                 <Switch>
                     <Route path="/list" exact component={List}></Route>
                     <Route path="/list/:id" component={Detail}></Route>
                 </Switch>
                </Router>
            </div>
        )
    }
}

export default ListContainer


