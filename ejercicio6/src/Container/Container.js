import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from 'react-router-dom';
import ListContainer from "../ListContainer/ListContainer";
import New from "../New/New";
import './Container.scss';
import 'bootstrap/dist/css/bootstrap.min.css';

class Container extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <Router>
                <div className="container">
                    <nav>
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/new">New</Link></li>
                        </ul>
                    </nav>
                    <Switch>
                        <Route path="/" exact >
                            <Redirect to="/list" />
                        </Route>
                        <Route path="/new">
                            <New />
                        </Route>
                        <Route path="/list">
                            <ListContainer />
                        </Route>
                    </Switch>
                </div>
            </Router>
        )
    }
}

export default Container
