import React from 'react';
import {
    Link,
} from 'react-router-dom';

class List extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            mensajes: []
        }
    }

    componentDidMount() {
        const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
        fetch(url, {
            method: 'get'
        })
            .then(res => res.json())
            .then((result) => {
                this.setState({
                    mensajes: result
                })
            })
    }

    handleClick(id) {
        this.props.history.push('/list/' + id);
    }

    getItems() {
        return (
            this.state.mensajes.map((mensaje) => {
                    return(
                        <li key={mensaje.id} className="card">
                            <div className="header">{mensaje.asunto}</div>
                            <div className="content">{mensaje.mensaje}</div>
                            <Link to={
                                {
                                    pathname: '/list/' + mensaje.id,
                                    state: {
                                        data: mensaje
                                    }
                                }
                            }>Pasar objeto</Link>
                            <button
                                className="btn btn-primary"
                                onClick={this.handleClick.bind(this, mensaje.id)}>Ver detalle</button>
                        </li>
                    )
                }
            )
        )
    }

    render(){
        return (
            <div className="list">
                <ul>
                    {this.getItems()}
                </ul>
            </div>
        )
    }
}

export default List
