import React from 'react';

class Detail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: '',
            mensaje: {
                asunto: '',
                mensaje: ''
            }
        }

        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        console.log(this.props);
        const location = this.props.location;
        if(location.state) {
            this.setState({
                mensaje: this.props.location.state.data,
                id: this.props.match.params.id
            })
        } else {
            this.setState({
                id: this.props.match.params.id
            });
        }
    }

    handleClick(){
        this.props.history.goBack();
    }

    render(){
        return (
            <div className="detail">
                <h3>Vista de detalle con id {this.state.id}</h3>
                <button className="btn btn-primary" onClick={this.handleClick}>Volver</button>

                <div className="header">{this.state.mensaje.asunto}</div>
                <div className="content">{this.state.mensaje.mensaje}</div>
            </div>
        )
    }
}

export default Detail
