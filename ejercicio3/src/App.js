
import './App.css';
import MainContainer from "./MainContainer/MainContainer";
import List from "./List/List";

function App() {
  return (
    <div className="App">
      <MainContainer />
    </div>
  );
}

export default App;
