import React from 'react';

class Toggle extends React.Component{
    TXT_MOSTRAR = 'mostrar';
    TXT_OCULTAR = 'ocultar';

    constructor(props) {
        super(props);

        this.state = {
            mostrar: true
        }

        this.cambiaMostrar = this.cambiaMostrar.bind(this);
    }

    cambiaMostrar() {
        const mostrar = !this.state.mostrar;
        this.setState({
            mostrar
        })
        this.props.handleToggle(mostrar);
    }

    render() {
        return(
            <button onClick={this.cambiaMostrar}>
                {
                    this.state.mostrar ?
                        this.TXT_OCULTAR
                        :
                        this.TXT_MOSTRAR
                }
            </button>
        )
    };
}
export default Toggle;
