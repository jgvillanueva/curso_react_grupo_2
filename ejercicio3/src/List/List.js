import React from 'react';
import Item from "../Item/Item";
import './List.css';

class List extends React.Component{

    constructor(props) {
        super(props);
    }

    render() {
        let items;
        if(this.props.usuarios) {
            items = this.props.usuarios.map((item) => {
                return <Item key={item.id} nombre={item.name} data={item}></Item>
            });
        }
        return(
            <ul className="lista" >
                {items}
            </ul>
        )
    };
}
export default List;
