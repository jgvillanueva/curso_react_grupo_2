import React from 'react';
import List from "../List/List";
import Toggle from "../Toggle/Toggle";

class MainContainer extends React.Component{
    usuarios = [
        {id: 1, name: "Jorge", descripcion: "kj sd32545345432 gñdfkjg"},
        {id: 2, name: "Ana", descripcion: "kj sdlk wret rtew ertbertwret rewtet gñdfkjg"},
        {id: 3, name: "Lucas", descripcion: "kj sdlk fdklgjdfskl gñdfkjg"},
        {id: 4, name: "Carmen", descripcion: "wretrewttrewb twtrekl gñdfkjg"},
        {id: 5, name: "Eva", descripcion: "er trewtertrwetwetert  rtewtttre tdfskl gñdfkjg"},
    ]

    constructor(props) {
        super(props);

        this.state = {
            mostrar: true,
        }

        this.toggleList = this.toggleList.bind(this);
    }

    toggleList(mostrar) {
        this.setState({
            mostrar
        })
    }

    render() {
        return(
            <div className="main-container" >
                <Toggle handleToggle={this.toggleList} />
                {
                    this.state.mostrar &&
                        <List usuarios={this.usuarios} />
                }
            </div>
        )
    };
}
export default MainContainer;
