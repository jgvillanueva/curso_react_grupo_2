import React from 'react';
import './Item.css';

class Item extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            ampliado: false
        }
    }

    toggleAmpliado(){
        const ampliado = !this.state.ampliado;
        this.setState({
            ampliado
        })
    }

    render() {
        return(
            <li onClick={this.toggleAmpliado.bind(this)} className="item">
                <div>
                    {this.props.nombre}
                </div>
                {
                    this.state.ampliado &&
                    (
                        <div>
                            {this.props.data.descripcion}
                        </div>
                    )
                }
            </li>
        )
    };
}
export default Item;
