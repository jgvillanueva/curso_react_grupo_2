
import './App.css';
import Counter from "./Counter/Counter";
import UserList from "./UserList/UserList";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Counter/>
        <UserList/>
      </header>
    </div>
  );
}

export default App;
