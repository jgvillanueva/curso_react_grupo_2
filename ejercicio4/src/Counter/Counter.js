import React, {useState} from 'react';

function Counter(props) {
    const [contador, setContador] = useState(0);

    const handleClick = () => {
        setContador(contador + 1);
    }

    return (
        <div>
            <h1>{contador}</h1>
            <button onClick={handleClick}>añade uno</button>
        </div>
    )
}

export default Counter;
