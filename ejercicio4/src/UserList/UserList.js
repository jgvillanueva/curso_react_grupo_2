import React, {useState, useEffect} from 'react';
import UserDetail from "../UserDetail/UserDetail";


function UserList(props) {
    const [users, setUsers] = useState([]);
    const [userSelected, setUserSelected] = useState(null);

    const getData = () => {
        const url = 'http://dev.contanimacion.com/api_tablon/api/users';
        fetch(url, {method: 'GET'})
            .then(res => res.json())
            .then((result) => {
                console.log(result);
                setUsers(result);
            })
    }

    useEffect(() => {
        getData();
    }, [])

    const handleClick = (userData) => {
        setUserSelected(userData);
    }

    return(
        <div>
            <ul>
                {
                    users.map((user) => {
                        return (
                            <li key={'user' + user.id}>
                                <span>{user.name} </span>
                                <button onClick={() => { handleClick(user)}}>Seleccionar</button>
                            </li>
                        )
                    })
                }
            </ul>
            <UserDetail userData={userSelected}/>
        </div>

    )
}
export default UserList;
