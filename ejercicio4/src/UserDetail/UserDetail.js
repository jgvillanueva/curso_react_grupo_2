import React, {useState, useEffect} from 'react';

function UserDetail(props) {
    const [historico, setHistorico] = useState([]);
    const {userData} = props;

    useEffect(() => {
        if (userData) {
            historico.push(userData.name);
            setHistorico(historico);
        }
    }, [userData]);


    if (userData) {
        return(
            <div>
                <div>Id: {userData.id}</div>
                <div>Nombre: {userData.name}</div>
                <div>
                    <h1>{historico.join(' ')}</h1>
                </div>
            </div>
        )
    } else {
        return (
            <div>no hay datos</div>
        )
    }
}
export default UserDetail;
