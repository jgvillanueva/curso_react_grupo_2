import React from 'react';

import PropTypes from 'prop-types';

class ErrorComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    render(){
        let msg;
        if (this.props.errorText !== '' && this.props.touched) {
            msg =
                <div className={'alert alert-' + this.props.colorAlert}>
                    {this.props.errorText}
                </div>
        }

        return (

            <div className="errorComponent my-3">
                {msg}
            </div>
        )
    }
}
export default ErrorComponent;


const ValicacionErrorText = (props, propName, componentName) => {
    if(props[propName]) {
        if (props[propName].length < 10) {
            return new Error(`${propName} en ${componentName} es demasiado corto `);
        } else {
            return null;
        }
    } else {
        return new Error(`${propName} en ${componentName} es obligatorio `);
    }
}

ErrorComponent.propTypes = {
    errorText: ValicacionErrorText,
    //errorText: PropTypes.string.isRequired,
    touched: PropTypes.bool.isRequired,
}

ErrorComponent.defaultProps = {
    colorAlert: 'danger'
}
