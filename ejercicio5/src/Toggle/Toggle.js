import React from 'react';
import './Toggle.css';

class Toggle extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            toggle: this.props.estadoIinical, // valores 1 y 2
        }

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(value) {
        this.setState({
            toggle: value,
        })
        this.props.parentHandler && this.props.parentHandler(value);
    }

    render(){
        return (
            <div className="toggle">
                <div
                    onClick={() => { this.handleClick(1) }}
                    className={this.state.toggle === 1 ? 'option active' : 'option disabled'}
                >
                    {this.props.opcion1}
                </div>
                <div
                    onClick={() => { this.handleClick(2) }}
                    className={this.state.toggle === 2 ? 'option active' : 'option disabled'}
                >
                    {this.props.opcion2}
                </div>
            </div>
        )
    }
}
export default Toggle;
