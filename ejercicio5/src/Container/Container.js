import React from 'react';
import Toggle from "../Toggle/Toggle";
import ControlledForm from "../ControlledForm/ControlledForm";
import UnControlledForm from "../UnControlledForm/UnControlledForm";


class Container extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tipo: 'Controlled',
        };

        this.handleToggle = this.handleToggle.bind(this);
    }

    handleToggle(value) {
        console.log('Llega el valor', value)
        if (value === 1) {
            this.setState({
                tipo: 'Controlled',
            })
        } else {
            this.setState({
                tipo: 'UnControlled',
            })
        }
    }

    render(){
        let form
        if(this.state.tipo === 'Controlled') {
            form = <ControlledForm />
        }  else {
            form = <UnControlledForm />
        }
        return (
            <div className="container">
                <Toggle
                    opcion1='Controlled'
                    opcion2='Uncontrolled'
                    estadoIinical={1}
                    parentHandler={this.handleToggle}
                />
                { form }
            </div>
        )
    }
}
export default Container;
