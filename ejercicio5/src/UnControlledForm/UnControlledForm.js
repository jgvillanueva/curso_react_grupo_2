import React from 'react';
import {useFormik} from 'formik';

const UnControlledForm = () => {

    const validateForm = (values)=> {
        const errors = {};
        if(!values.asunto) {
            errors.asunto = 'El campo asunto es obligatorio';
        } else if(values.asunto.length < 5) {
            errors.asunto = 'El campo asunto debe tener más de 5 caracteres';
        }
        if(!values.mensaje) {
            errors.mensaje = 'El campo mensaje es obligatorio';
        } else if(values.mensaje.length < 15) {
            errors.mensaje = 'El campo mensaje debe tener más de 15 caracteres';
        }

        return errors;
    }

    const formik = useFormik({
        initialValues: {
            asunto: '11111',
            mensaje: '22222'
        },
        onSubmit: (values) => {
           console.log('Los valores a enviar son:', values) ;

        },
        validate: validateForm,
    });

    return (
        <div className='card'>
            <div className="card-header">
                <h1>UnControlled form</h1>
            </div>
            <div className="card-body">
                <form className='formulario' onSubmit={formik.handleSubmit}>
                    <div className='form-group' >
                        <label htmlFor='email'>Asunto</label>
                        <input
                            type='text'
                            className='form-control'
                            name='asunto'
                            value={formik.values.asunto}
                            onChange={formik.handleChange}
                        />
                        {
                            formik.errors.asunto ?
                                <div className="alert alert-danger">{formik.errors.asunto}</div>
                                :
                                null
                        }
                    </div>
                    <div className='form-group' >
                        <label htmlFor='mensaje'>Mensaje</label>
                        <textarea
                            className='form-control'
                            name='mensaje'
                            value={formik.values.mensaje}
                            onChange={formik.handleChange}
                        ></textarea>
                        {
                            formik.errors.mensaje ?
                                <div className="alert alert-danger">{formik.errors.mensaje}</div>
                                :
                                null
                        }
                    </div>
                    <div className='form-group' >
                        <label htmlFor='user_id'>User id</label>
                        <select
                            className='form-control'
                            name='user_id'
                        >
                            <option value="11">Carmen</option>
                            <option value="12">Claudia</option>
                            <option value="13">Jorge</option>
                            <option value="14">David</option>
                        </select>
                    </div>
                    <button type="submit" className="btn btn-primary" >
                        Enviar
                    </button>
                </form>
            </div>
        </div>
    )

}
export default UnControlledForm;





/*
import React, { Component } from 'react';

class UnControlledForm extends Component {
    asuntoInput
    mensajeInput

    constructor(props) {
        super(props);

        this.asuntoInput = React.createRef();
        this.mensajeInput = React.createRef();
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log(this.asuntoInput.current.value, this.mensajeInput.current.value,);
    }

    render() {
        return (
            <div className='card'>
                <div className="card-header">
                    <h1>UnControlled form</h1>
                </div>
                <div className="card-body">
                    <form className='formulario' onSubmit={this.handleSubmit.bind(this)} >
                        <div className='form-group' >
                            <label htmlFor='email'>Asunto</label>
                            <input
                                type='text'
                                className='form-control'
                                name='asunto'
                                ref={this.asuntoInput}
                            />
                        </div>
                        <div className='form-group' >
                            <label htmlFor='mensaje'>Mensaje</label>
                            <textarea
                                className='form-control'
                                name='mensaje'
                                ref={this.mensajeInput}
                            ></textarea>
                        </div>
                        <div className='form-group' >
                            <label htmlFor='user_id'>User id</label>
                            <select
                                className='form-control'
                                name='user_id'
                            >
                                <option value="11">Carmen</option>
                                <option value="12">Claudia</option>
                                <option value="13">Jorge</option>
                                <option value="14">David</option>
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary" >
                            Enviar
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}
export default UnControlledForm;
*/
