import React, { Component } from 'react';
import ErrorComponent from "../ErrorComponent/ErrorComponent";

class ControlledForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mensaje: {
                asunto: '',
                mensaje: '',
                user_id: 12
            },
            formErrors: {
                asunto: 'No se ha introducido valor',
                mensaje: 'No se ha introducido valor',
            },
            formTouched: {
                asunto: false,
                mensaje: false,
            },
            formValid: false,
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.validateField = this.validateField.bind(this);
    }

    handleBlur(event) {
        let formtouched = this.state.formTouched;
        formtouched[event.target.name] = true;
        this.setState({
            formtouched
        })
    }

    handleChange(event) {
        const mensaje = this.state.mensaje;
        mensaje[event.target.name] = event.target.value;
        this.setState({
            mensaje
        }, () => { this.validateField(event.target.name, event.target.value)} )
    }

    handleSubmit(event){
        event.preventDefault();
        console.log('los valores a enviar son', this.state.mensaje);
    }

    validateField(fieldName, fieldValue) {
        let formErrors = this.state.formErrors;
        let valid;
        let message;
        switch (fieldName) {
            case 'asunto':
                valid = fieldValue.length > 5;
                message = valid ? '' : 'El asunto ha de tener más de 5 caracteres';
                break;
            case 'mensaje':
                valid = fieldValue.length > 15;
                message = valid ? '' : 'El mensaje ha de tener más de 15 caracteres';
                break;
        }
        formErrors[fieldName] = message;

        let formValid = true;
        Object.keys(formErrors).forEach((errorName) => {
            if (formErrors[errorName] !== '' ) {
                formValid = false;
            }
        })

        this.setState({
            formErrors,
            formValid,
        })
    }

    render() {
        return (
            <div className='card'>
                <div className="card-header">
                    <h1>Controlled form</h1>
                </div>
                <div className="card-body">
                    <form className='formulario' onSubmit={this.handleSubmit} >
                        <div className='form-group' >
                            <label htmlFor='email'>Asunto</label>
                            <input
                                type='text'
                                className='form-control'
                                name='asunto'
                                value={this.state.mensaje.asunto}
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                            />
                            <ErrorComponent
                                errorText={this.state.formErrors.asunto}
                                touched={this.state.formTouched.asunto}
                            />
                        </div>
                        <div className='form-group' >
                            <label htmlFor='mensaje'>Mensaje</label>
                            <textarea
                                className='form-control'
                                name='mensaje'
                                value={this.state.mensaje.mensaje}
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                            ></textarea>
                            <ErrorComponent
                                colorAlert='warning'
                                errorText={this.state.formErrors.mensaje}
                                            touched={this.state.formTouched.mensaje}/>
                        </div>
                        <div className='form-group' >
                            <label htmlFor='user_id'>User id</label>
                            <select
                                className='form-control'
                                name='user_id'
                                value={this.state.mensaje.user_id}
                                onChange={this.handleChange}
                            >
                                <option value="11">Carmen</option>
                                <option value="12">Claudia</option>
                                <option value="13">Jorge</option>
                                <option value="14">David</option>
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary" disabled={!this.state.formValid} >
                            Enviar
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}
export default ControlledForm;
